from os import environ
import requests
import logging
import json


# This needs to be filled with the Page Access Token that will be provided
# by the Facebook App that will be created.
PAT = environ['FB_ACCESS_TOKEN']


def send_message(token, recipient, text):
    """
    Send the message text to recipient with id recipient.
    """
    r = requests.post("https://graph.facebook.com/v2.6/me/messages",
        params={"access_token": token},
        data=json.dumps({
            "recipient": {"id": recipient},
            "message": {"text": text.decode('unicode_escape')}
        }),
        headers={'Content-type': 'application/json'})
    if r.status_code != requests.codes.ok:
        logging.error(r.text)


def get_commands(message):
    """
    Recognizes things like "from" or "to" in the message.
    """
    result = {}

    tokens = message.split(' ')
    for i, token in enumerate(tokens[:-1]):
        result[token] = tokens[i+1]

    return result


def callback(sender, message):
    """
    Handles a given message from the given sender.
    """
    commands = get_commands(message)

    answer = "I recognize, you want to go from {} to {} via {}.".format(
        commands.get('from', '(not given)'),
        commands.get('to', '(not given)'),
        commands.get('via', '(not given)'))
    send_message(PAT, sender, answer)
